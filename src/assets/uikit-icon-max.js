<!DOCTYPE html><html lang="en-gb" dir="ltr" vocab="http://schema.org/"><head>
      <!--link href="http://demo.openaire.eu" rel="canonical" /-->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <base href="overview.html">
      <meta http-equiv="content-type" content="text/html; charset=utf-8">
      <meta name="description" content="open access, research, scientific publication, European Commission, EC, FP7, ERC, Horizon 2020, H2020, search, projects ">

      <meta property="og:description" content="open access, research, scientific publication, European Commission, EC, FP7, ERC, Horizon 2020, H2020, search, projects ">
      <meta property="og:title" content="Search OpenAIRE">
      <meta property="og:type" content="website">
      <meta property="og:url" content="https://demo.openaire.eu">
      <meta property="og:site_name" content="OpenAIRE">

      <meta property="og:image" content="https://demo.openaire.eu/assets/dl119_files/Symbol.png">
      <meta property="og:image:secure_url" content="https://demo.openaire.eu/assets/dl119_files/Symbol.png">
      <meta property="og:image:type" content="image/png">
      <meta property="og:image:width" content="360">
      <meta property="og:image:height" content="359">

      <link href="assets/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
      <title>OpenAIRE | Error page</title>
      <script>
      window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
        // console.log(errorMsg + " __" + url + " __" +  lineNumber + " __" +  column + " __" +  errorObj);
        if(errorMsg.indexOf("uikit.js") != -1 || url.indexOf("uikit.js") != -1 ){
          console.log("********UIKIT Error ***********");
          $.getScript("assets/dl119_files/uikit.js");
          $.getScript("assets/dl119_files/uikit-icons-max.js");
}

}
      </script>
       <!-- <script src="assets/jquery/jquery.min.js" type="text/javascript"></script> -->
       <script src="assets/dl119_files/jquery.js"></script>
       <!-- <script  src="assets/dl119_files/uikit.js"></script> -->
       <script src="assets/dl119_files/uikit-icons-max.js"></script>
<script>
$(document).ready(function(){
  console.log("Is ready - load uikit ***")
    $.getScript("assets/dl119_files/uikit.js");
    $.getScript("assets/dl119_files/uikit-icons-max.js");
});
</script>
       <!-- <script  src="assets/dl119_files/uikit.js"></script> -->
       <!-- <script src="assets/dl119_files/uikit-icons-max.js"></script> -->


      <!-- <link rel="stylesheet" href="node_modules/uikit/dist/css/uikit.min.css"> -->
      <link rel="stylesheet" href="assets/dl119_files/theme.css">
      <link rel="stylesheet" href="assets/dl119_files/custom.css">
      <link rel="stylesheet" href="assets/custom.css">

      <!-- <script async src="assets/dl119_files/theme.js"></script> -->

      <!--script src="node_modules/uikit/dist/js/uikit.min.js"></script-->

             <!-- Google sitename markup-->
      <script type="application/ld+json">
         {
           "@context" : "http://schema.org",
           "@type" : "WebSite",
           "name" : "OpenAIRE",
             "url" : "http://demo.openaire.eu"
         }
      </script>
      <!-- End of Google sitename markup-->
      <!-- Google sitelinks search markup-->
      <script type="application/ld+json">
         {
           "@context": "http://schema.org",
           "@type": "WebSite",
           "url" : "http://demo.openaire.eu",
           "potentialAction": {
             "@type": "SearchAction",
             "target": "http://demo.openaire.eu/search/find/?keyword={search_term_string}",
             "query-input": "required name=search_term_string"
           }
         }
      </script>

      <script src="//cdn.jsdelivr.net/clipboard.js/1.5.16/clipboard.min.js"></script>
      <!--script type='text/javascript' src="node_modules/clipboard/dist/clipboard.min.js"></script-->
      <!--script src="https://cdn.jsdelivr.net/clipboard.js/1.5.12/clipboard.min.js"></script-->

      <!--script type='text/javascript' src='https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js'></script-->

      <!-- End of Google sitelinks search markup-->

      <script src="../node_modules/angular2/bundles/angular2-polyfills.js"></script>

 <style></style></head>
 <body class="" style="">

   <div class="uk-offcanvas-content uk-height-viewport">
     <script async="" src="/main.bundle.js"></script>

      <app _nghost-5ee2-1="">
        <navbar _ngcontent-5ee2-1="">



  <div class="tm-header-mobile uk-hidden@m">
     <nav class="uk-navbar-container uk-navbar" uk-navbar="">
        <div class="uk-navbar-left">
           <a class="uk-navbar-toggle" href="#tm-mobile" uk-toggle="">
              <div class="uk-navbar-toggle-icon uk-icon custom-navbar-toggle-icon" uk-navbar-toggle-icon=""> </div>
           </a>
        </div>
        <div class="uk-navbar-center">
           <a class="uk-navbar-item uk-logo" routerLink="/search/find" routerLinkActive="uk-link" href="/search/find">
           <img alt="OpenAIRE" class="uk-responsive-height" src="assets/OA DISCOVER_A.png">        </a>
        </div>
        <div class="uk-navbar-right">
           <user-mini>

        <!--template bindings={}-->

    </user-mini>
        </div>
     </nav>
     <div class="uk-offcanvas" id="tm-mobile" mode="slide" overlay="" uk-offcanvas="">
        <div class="uk-offcanvas-bar">
           <button class="uk-offcanvas-close uk-close uk-icon" type="button" uk-close=""></button>
           <div class="uk-child-width-1-1 uk-grid" uk-grid="">
              <div>
                 <!--template bindings={}-->
              </div>
              
           </div>
        </div>
     </div>
  </div>
  <div class="tm-toolbar custom-discover-toolbar uk-visible@m">
     <div class="uk-container uk-flex uk-flex-middle uk-container-expand">
        <div class="uk-margin-auto-left">
           <div class="uk-grid-medium uk-child-width-auto uk-flex-middle uk-grid uk-grid-stack" uk-grid="margin: uk-margin-small-top">
              <div class="uk-first-column">
                 <div class="uk-panel inner" id="module-119">
                    <ul class="uk-subnav uk-subnav-line">
                       <li class="uk-active"><a href="https://beta.openaire.eu"><img alt="home" class="uk-responsive-height" src="assets/dl119_files/Home-icon.png"></a></li>
                       <li class="custom-discover-li"><a routerLink="/search/find" routerLinkActive="uk-link" href="/search/find">Discover</a></li>
                       <li><a>Join</a></li>
                       <li><a>Connect</a></li>
                       <li><a>Monitor</a></li>
                       <li><a>Develop</a></li>
                    </ul>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>
  <div class="tm-header uk-visible@m tm-header-transparent" uk-header="">
     <div animation="uk-animation-slide-top" class="uk-navbar-container uk-sticky uk-navbar-transparent" cls-active="uk-active uk-navbar-sticky" cls-inactive="uk-navbar-transparent uk-light" media="768" style="" top=".tm-header + [class*=&quot;uk-section&quot;]" uk-sticky="">
        <div class="uk-container uk-container-expand">
           <nav class="uk-navbar" uk-navbar="{&quot;align&quot;:&quot;left&quot;}">
              <div class="uk-navbar-left  uk-visible@l  ">
                 <a class="uk-logo uk-navbar-item" routerLink="/search/find" routerLinkActive="uk-link" href="/search/find">
                 <img alt="OpenAIRE" class="uk-responsive-height" src="assets/OA DISCOVER_B.png"></a>
              </div>
              <div class="uk-navbar-left  uk-visible@m uk-hidden@l">
                 <a class="uk-logo uk-navbar-item" routerLink="/search/find" routerLinkActive="uk-link" href="/search/find">
                 <img alt="OpenAIRE" class="uk-responsive-height" src="assets/OA DISCOVER_A.png"></a>
              </div>
              <div class="uk-navbar-center">
                 <!--template bindings={}-->
              </div>
              <div class="uk-navbar-right">
                 <user-mini>

        <!--template bindings={}-->

    </user-mini>
              </div>
           </nav>
        </div>
     </div>
     
  </div>
  <div class="first_page_section uk-section-default uk-section uk-padding-remove-vertical">
     <div class="first_page_banner_headline uk-grid-collapse uk-flex-middle uk-margin-remove-vertical uk-grid uk-grid-stack" uk-grid="">
     </div>
  </div>








</navbar>



          
                   <div _ngcontent-5ee2-1="" class="custom-main-content">
                      <main _ngcontent-5ee2-1="">
                       <router-outlet _ngcontent-5ee2-1=""></router-outlet><error>
    <div class=" uk-section  uk-margin-small-top tm-middle" id="tm-main">
      <div uk-grid="">
       <div class="tm-main uk-width-1-1@s uk-width-1-1@m  uk-width-1-1@l uk-row-first ">

        <div class="uk-container">
            <h2>
                Bad karma: we can't find that page!
            </h2>
            <br>

            <p>
                You asked for /assets/dl119_files/uikit-icon-max.js, but despite our computers looking very hard, we could not find it. What happened ?
            </p>

            <ul>
                <li>the link you clicked to arrive here has a typo in it</li>
                <li>or somehow we removed that page, or gave it another name</li>
                <li>or, quite unlikely for sure, maybe you typed it yourself and there was a little mistake ?</li>
            </ul>

        </div>
      </div>
     </div>
  </div>
    </error>
                     </main>
                    </div>
                   
          <!--template bindings={}-->
          <!--template bindings={}-->
           <!--template bindings={}-->

</app>
    </div>

        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <!-- to update addThis: https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-559d24521cd8c080-->
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-559d24521cd8c080"></script>
  <script type="text/javascript">
  function loadAltmetrics(e,t,n){
    var d="createElement",c="getElementsByTagName",m="setAttribute",n=document.getElementById(e);
    return n&&n.parentNode&&n.parentNode.removeChild(n),n=document[d+"NS"]&&document.documentElement.namespaceURI,n=n?document[d+"NS"](n,"script"):document[d]("script"),n[m]("id",e),n[m]("src",t),(document[c]("head")[0]||document[c]("body")[0]).appendChild(n),n=new Image,void n[m]("src","https://d1uo4w7k31k5mn.cloudfront.net/donut/0.png")
  };
</script>

   

<universal-script><script>
 try {window.UNIVERSAL_CACHE = ({"APP_ID":"5ee2","CacheService":"{}"}) || {};} catch(e) {  console.warn("Angular Universal: There was a problem parsing data from the server")}
</script></universal-script></body></html>