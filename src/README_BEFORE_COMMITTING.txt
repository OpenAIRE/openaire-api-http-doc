@author: Alessia Bardi
@date: 28 September 2018
@title: Info on how to update documentation and changelog for api.openaire.eu

1)---- DOCUMENTATION UPDATE ---
You just need to simply modify the html files on your local copy of https://svn.driver.research-infrastructures.eu/driver/dnet45/modules/dnet-api-http-doc/

2)---- READ THIS BEFORE COMMITTING ON SVN ----
Do you want your log message to be visible on http://api.openaire.eu/changelog.html?
	Yes --> Then commit with a nice message that will be publicly available and go to step 3 (example: new API parameter, new schema version)
	No --> Start your log message with the keyword PRIVATE (capital letters) and go to step 4 (typo fixed, changes to this file)

3)---- UPDATE changelog.html ----
So, you have committed and you want to update the changelog so that your log message will appear in http://api.openaire.eu/changelog.html.
To generate an updated changelist of this documentation as html page, run the following commands on your local machine:

	cd dnet-api-http-doc/src
	svn log -v --xml https://svn.driver.research-infrastructures.eu/driver/dnet45/modules/dnet-api-http-doc | xsltproc svnlog2html.xslt - > ./changelog.html
	svn commit -m "PRIVATE : updated changelist"

Now you can go to step 4

4)---- PUBLISH UPDATED DOCUMENTATION ----
Login via ssh to www-farm.openaire.eu and update the documentation folder /srv/www/static/graph/develop/:

	cd /srv/www/static/graph/develop
	svn up

Remember to go to graph.openaire.eu/develop and double check the changes appear as you expect. If not, double check you committed everything.
If you have problems with the commit procedure or svn up on the server folder contact Alessia Bardi.
If you have problems with the layout or visualization of figures, contact Argiro Kokogiannaki.
